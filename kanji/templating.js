if (!String.prototype.startsWith) {
  String.prototype.startsWith = function(searchString, position) {
    position = position || 0;
    return this.indexOf(searchString, position) === position;
  };
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

function getKanjiByHeisigChapter(){
  var startChapter = document.getElementById("hchapstart").value;
  var endChapter = document.getElementById("hchapend").value;

  var startIndex = heisigChapterIndices[startChapter-1];
  var endIndex = heisigChapterIndices[endChapter];

  fillKanji(heisig.slice(startIndex,endIndex));
}

function getKanjiByHeisigIndex(){
  var a = document.getElementById("hindstart").value;
  var b = document.getElementById("hindend").value;
  fillKanji(heisig.slice(a,parseInt(b)+1));
}

function getKanjiBySchoolGrade(){
  var startGrade = document.getElementById("gstart").value;
  var endGrade = document.getElementById("gend").value;

  var startIndex = schoolGradeIndices[startGrade-1];
  var endIndex = schoolGradeIndices[endGrade];

  fillKanji(schoolKanji.slice(startIndex,endIndex));
}

document.getElementById("jstart").addEventListener("change", function(event){
  var startGrade = document.getElementById("jstart").value;
  var endGrade = document.getElementById("jend").value;

  if(parseInt(startGrade) < parseInt(endGrade)){
    document.getElementById('jend').value = startGrade;
  }
});

document.getElementById("jend").addEventListener("change", function(event){
  var startGrade = document.getElementById("jstart").value;
  var endGrade = document.getElementById("jend").value;

  if(parseInt(endGrade) > parseInt(startGrade)){
    document.getElementById('jstart').value = endGrade;
  }
});

document.getElementById("gstart").addEventListener("change", function(event){
  var startGrade = document.getElementById("gstart").value;
  var endGrade = document.getElementById("gend").value;

  if(parseInt(startGrade) > parseInt(endGrade)){
    document.getElementById('gend').value = startGrade;
  }
});

document.getElementById("gend").addEventListener("change", function(event){
  var startGrade = document.getElementById("gstart").value;
  var endGrade = document.getElementById("gend").value;

  if(parseInt(endGrade) < parseInt(startGrade)){
    document.getElementById('gstart').value = endGrade;
  }
});

function getKanjiByJLPTGrade(){
  var startGrade = document.getElementById("jstart").value;
  var endGrade = document.getElementById("jend").value;

  var kanji = "";
  if(5 <= startGrade && 5>= endGrade){
    kanji += N5Kanji;
  }
  if(4 <= startGrade && 4>= endGrade){
    kanji += N4Kanji;
  }
  if(3 <= startGrade && 3>= endGrade){
    kanji += N3Kanji;
  }
  if(2 <= startGrade && 2>= endGrade){
    kanji += N2Kanji;
  }
  if(1 <= startGrade && 1>= endGrade){
    kanji += N1Kanji;
  }
  fillKanji(kanji);
}

document.getElementById("char-repeat-times").addEventListener("change", function(e){
  if(parseInt(document.getElementById("char-repeat-times").value) == 1){
    document.getElementById("char-repeat-plural").classList.add("hidden");
  } else {
    document.getElementById("char-repeat-plural").classList.remove("hidden");
  }
})

document.getElementById("extra-vertical-guidelines").addEventListener("change", function(e){
  if(document.getElementById("extra-vertical-guidelines").checked){
    document.querySelectorAll(".sline").forEach(x => x.classList.add("hidden"));
    document.querySelectorAll(".scell").forEach(x => x.classList.remove("hidden"));
  } else {
    document.querySelectorAll(".scell").forEach(x => x.classList.add("hidden"));
    document.querySelectorAll(".sline").forEach(x => x.classList.remove("hidden"));
  }
});

function getKanjiFromInput(){
  var input = document.getElementById("japaneseinput").value;

  var selectedCharSet = document.querySelector('input[name="charset"]:checked').value;
  if(selectedCharSet == "Kanji"){
    input = input.replace(/[^\u4E00-\u9FAF\uFF66-\uFF9D\u31F0-\u31FF]/g,""); //Strip everything that isn't a kanji.
  } else if (selectedCharSet == "Kana"){
    input = input.replace(/[^\u3040-\u30FA]/g,""); //Strip everything that isn't a hiragana/katakana character.
  } else {
    input = input.replace(/[^\u3040-\u30FA\u4E00-\u9FAF\uFF66-\uFF9D\u31F0-\u31FF]/g,""); //Strip everything that isn't a hiragana/katakana or kanji character.
  }

  fillKanji(input);
}

var template1 = '<div class="grid-container" style="width:100%">\
  <div class="lcell"><img class="strokediagram" src="IMGURL"></div>\
  <div class="lcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="sline"><span class="characterholder">CHARACTER</span></div>\
  <div class="sline"><span class="characterholder">CHARACTER</span></div>\
  <div class="sline"><span class="characterholder">CHARACTER</span></div>\
</div>';

var template2 = '<div class="grid-container2 gothic">\
  <div class="lcell"><img class="strokediagram" src="IMGURL"></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="sline"><span class="characterholder">CHARACTER</span></div>\
  <div class="sline"><span class="characterholder">CHARACTER</span></div>\
  <div class="sline"><span class="characterholder">CHARACTER</span></div>\
</div>';

var template3 = '<div class="grid-container3 gothic">\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"><span class="characterholder">CHARACTER</span></div>\
  <div class="mcell"></div>\
  <div class="mcell"></div>\
  <div class="sline"><span class="characterholder">CHARACTER</span></div>\
</div>';

function fillKanji(input){

  //Making it unique
  input = input.split('').filter(function(item, i, ar){ return ar.indexOf(item) === i; }).join('');

  var repeats = parseInt(document.getElementById("char-repeat-times").value);

  var repeatedInput = input;
  while(repeats > 1){
    input = input + repeatedInput;
    repeats -= 1;
  }

  var selectedSorting = document.querySelector('input[name="sorting"]:checked').value;
  if(selectedSorting == "Heisig"){
    input = input.split('').sort().sort(compareKanji).join(""); //Sorting on Heisig order
  } else if (selectedSorting == "Number of strokes"){
    input = input.split('').sort().sort(compareNumStrokes).join("");
  } else if (selectedSorting == "Unicode"){
    input = input.split("").sort().join(""); //Sorting it on Unicode value
  } else if (selectedSorting == "Shuffle"){
    var arr = input.split('');
    shuffle(arr);
    input = arr.join("");
  }

  var outputDocument = "";

  var template = "";

  var selectedTemplate = document.querySelector('input[name="template"]:checked').value;
  if(selectedTemplate == "1"){
    template = document.querySelector(".grid-container").outerHTML;
  } else if (selectedTemplate == "2"){
    template = document.querySelector(".grid-container2").outerHTML;
  } else {
    template = document.querySelector(".grid-container3").outerHTML;
  }

  for(i=0;i<input.length;i++){
    var character = input.charAt(i);
    var charCode = input.charCodeAt(i).toString(16);
    if(charCode.length == 4){
      charCode = '0' + charCode;
    }

    var fileName = charCode + '.svg';

    outputDocument = outputDocument + template
        .replace(/[\u4E00-\u9FAF\uFF66-\uFF9D\u31F0-\u31FF]/g,character)
        .replace(/[0-9a-f]+\.svg/g,fileName);
  }

  document.getElementById("writingGrid").innerHTML = outputDocument;
}

function compareKanji(a,b){
  if(kanjiDic[a] === undefined || kanjiDic[a].h===undefined){
    return 1;
  } else if (kanjiDic[b] === undefined || kanjiDic[b].h===undefined){
    return -1;
  } else {
    return kanjiDic[a].h - kanjiDic[b].h;
  }
}

function compareNumStrokes(a,b){
  if(kanjiDic[a] === undefined || kanjiDic[a].s===undefined){
    return 1;
  } else if (kanjiDic[b] === undefined || kanjiDic[b].s===undefined){
    return -1;
  } else {
    return kanjiDic[a].s - kanjiDic[b].s;
  }
}

function checkFontChoice(){
  var selectedFont = document.querySelector('input[name="fontchoice"]:checked').value;
  if(selectedFont=="Handwritten"){
    handwritten();
  } else if(selectedFont == "Gothic"){
      gothic();
  } else if(selectedFont == "Mincho") {
      mincho();
  } else {
    kanjivg();
  }
}

function gothic() {
  //repImgsChars();
  var selects = document.querySelectorAll(".mincho, .gothic, .handwritten, .kanjivg");
  var l = selects.length;
  for(var i = 0; i < l;i++){
    selects[i].classList.add("gothic");
    selects[i].classList.remove("mincho");
    selects[i].classList.remove("handwritten");
    selects[i].classList.remove("kanjivg");
  }
}

function mincho() {
  //repImgsChars();
  var selects = document.querySelectorAll(".mincho, .gothic, .handwritten, .kanjivg");
  var l = selects.length;
  for(var i = 0; i < l;i++){
    selects[i].classList.add("mincho");
    selects[i].classList.remove("gothic");
    selects[i].classList.remove("handwritten");
    selects[i].classList.remove("kanjivg");
  }
}

function handwritten(){
  //repCharsImgs();
  var selects = document.querySelectorAll(".mincho, .gothic, .handwritten, .kanjivg");
  var l = selects.length;
  for(var i = 0; i < l;i++){
    selects[i].classList.add("handwritten");
    selects[i].classList.remove("gothic");
    selects[i].classList.remove("mincho");
    selects[i].classList.remove("kanjivg");
  }
}

function kanjivg() {
  //repImgsChars();
  var selects = document.querySelectorAll(".mincho, .gothic, .handwritten, .kanjivg");
  var l = selects.length;
  for(var i = 0; i < l;i++){
    selects[i].classList.add("kanjivg");
    selects[i].classList.remove("mincho");
    selects[i].classList.remove("gothic");
    selects[i].classList.remove("handwritten");
  }
}