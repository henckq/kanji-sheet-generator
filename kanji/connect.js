var HTMLescapes = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

var HTMLunescapes = {
    '&amp;': '&',
    '&lt;': '<',
    '&gt;': '>',
    '&quot;': '"',
    '&#39;': "'",
    '&#x2F;': '/',
    '&#x60;': '`',
    '&#x3D;': '='
};
  
function escapeHTML (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return HTMLescapes[s];
    });
}

function unescapeHTML (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return HTMLunescapes[s];
    });
}

function ankiRequest(action, version, params={}) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.addEventListener('error', () => reject('failed to connect to AnkiConnect'));
        xhr.addEventListener('load', () => {
            try {
                const response = JSON.parse(xhr.responseText);
                if (response.error) {
                    throw response.error;
                } else {
                    if (response.hasOwnProperty('result')) {
                        resolve(response.result);
                    } else {
                        reject('failed to get results from AnkiConnect');
                    }
                }
            } catch (e) {
                reject(e);
            }
        });

        xhr.open('POST', 'http://127.0.0.1:8765');
        xhr.send(JSON.stringify({action, version, params}));
    });
}

async function fetchDeckNames(){
    try {
    var decks = await ankiRequest('deckNames',5);
    } catch(err) {
        document.getElementById("ankiconnecterror").style.display = "inline";
        return;
    }
    document.getElementById("ankiconnecterror").style.display = "none";
    decks.sort();
    var options = "";
    for(i=0;i<decks.length;i++){
        options += "<option value=\"{0}\">{0}</option>\n".replace(/\{0\}/g,escapeHTML(decks[i]));
    }

    document.getElementById("selector").innerHTML="<br> Deck to fetch from: <select name=\"deck\" id=\"deckSelector\">\n"+options+"</select>\n";
    //document.getElementById("queryButtons").style="display:inline;";
    document.getElementById("queryButtons").style.display = "inline";
}

async function dueToday(){
    const deckSelector = document.getElementById("deckSelector");
    const selectedDeck = deckSelector.options[deckSelector.selectedIndex].text;
    const fieldName = document.getElementById("fieldName").value;

    const noteIDs = await ankiRequest('findNotes',5,{'query':'deck:"\{0\}" is:due'.replace(/\{0\}/g,unescapeHTML(selectedDeck))});
    const notes = await ankiRequest('notesInfo',5,{'notes':noteIDs});

    var characters = "";
    for(i=0;i<noteIDs.length;i++){
        if(notes[i]['fields'][fieldName] != undefined){
            characters += notes[i]['fields'][fieldName].value;
        }
    }
    
    document.getElementById("japaneseinput").value = characters;
    getKanjiFromInput();
}

async function pullEntireDeck(){
    const deckSelector = document.getElementById("deckSelector");
    const selectedDeck = deckSelector.options[deckSelector.selectedIndex].text;
    const fieldName = document.getElementById("fieldName").value;
    const noteIDs = await ankiRequest('findNotes',5,{'query':'deck:"\{0\}"'.replace(/\{0\}/g,unescapeHTML(selectedDeck))});
    const notes = await ankiRequest('notesInfo',5,{'notes':noteIDs});

    var characters = "";
    for(i=0;i<noteIDs.length;i++){
        if(notes[i]['fields'][fieldName] != undefined){
            characters += notes[i]['fields'][fieldName].value;
        }
    }
    
    document.getElementById("japaneseinput").value = characters;
    getKanjiFromInput();
}

async function customQuery(){
    const customQuery = document.getElementById("customQuery").value;
    const fieldName = document.getElementById("fieldName").value;
    
    const noteIDs = await ankiRequest('findNotes',5,{'query':customQuery});
    const notes = await ankiRequest('notesInfo',5,{'notes':noteIDs});

    var characters = "";
    for(i=0;i<noteIDs.length;i++){
        if(notes[i]['fields'][fieldName] != undefined){
            characters += notes[i]['fields'][fieldName].value;
        }
    }
    
    document.getElementById("japaneseinput").value = characters;
    getKanjiFromInput();
}